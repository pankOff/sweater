#!/usr/bin/env bash

mvn clean package

echo 'Copy files...'

scp -i ~/.ssh/SSH__KEY__TO__AWS__TEST__INSTANCE.pem \
      target/sweater-1.0-SNAPSHOT.jar \
      ubuntu@34.213.44.207:/home/ubuntu/

echo 'Restart server...'

ssh -i ~/.ssh/SSH__KEY__TO__AWS__TEST__INSTANCE.pem ubuntu@34.213.44.207 << EOF

pgrep java | xargs kill -9
nohup java -jar sweater-1.0-SNAPSHOT.jar > log.txt &

EOF

echo 'Bye'