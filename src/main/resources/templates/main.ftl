<#import "parts/common.ftl" as c>

<@c.page>

<div class="form-row">
    <div class="form-group col-md-6">
        <form method="get" action="/main" class="form-inline">
            <input type="text" name="filter" class="form-control mr-2" placeholder="Search by tag" value="${filter!}">
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>
</div>

<a class="btn btn-primary mb-2" data-toggle="collapse" href="#collapseMessage" role="button" aria-expanded="false" aria-controls="collapseMessage">
    Add new message
</a>

<div class="collapse <#if message??>show</#if>" id="collapseMessage">
    <div class="form-group col-md-6">
        <form method="post" enctype="multipart/form-data">
            <input class="form-control mb-1 ${(textError??)?string('is-invalid','')}"
                   value="<#if message??>${message.text}</#if>" type="text" name="text" placeholder="Enter message" />
            <#if textError??>
                <div class="invalid-feedback">
                    ${textError}
                </div>
            </#if>

            <input type="text" class="form-control mb-1"
                   value="<#if message??>${message.tag}</#if>" name="tag" placeholder="Тэг">
            <#if tagError??>
                <div class="invalid-feedback">
                    ${tagError}
                </div>
            </#if>
            <div class="custom-file mb-1">
                <input type="file" name="file" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}" />
            <button class="btn btn-primary" type="submit">Post</button>
        </form>
    </div>

</div>

<div class="card-columns">
<#list messages as message>
    <div class="card my-3">
        <#if message.filename??>
            <img src="/img/${message.filename}" class="card-img-top">
        </#if>
        <div class="m-2">
            <span>${message.text}</span>
            <i>${message.tag}</i>
        </div>
        <div class="card-footer text-muted">
            ${message.authorName}
        </div>
    </div>
<#else>
No messages
</#list>
</div>
</@c.page>